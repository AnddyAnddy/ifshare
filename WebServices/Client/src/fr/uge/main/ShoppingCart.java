package fr.uge.main;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map.Entry;

import com.currencysystem.webservices.currencyserver.CurncsrvReturnRate;
import com.currencysystem.webservices.currencyserver.CurrencyServerSoap;

import fr.uge.bank.Bank;
import fr.uge.ifService.IfService;

public class ShoppingCart {
	private final HashMap<Integer, Integer> cart = new HashMap<>();
	private final IfService service;
	private final Bank bank;
	private final CurrencyServerSoap server;

	public ShoppingCart(IfService service, Bank bank, CurrencyServerSoap server) {
		this.service = service;
		this.bank = bank;
		this.server = server;
	}

	private String splitRemoteExceptionMessage(String message) {
		return message.split(": ")[1];
	}

	public void addProduct(int id, int quantity) {
		if (quantity <= 0) {
			System.out.println("Quantity must be positive : " + quantity);
		} else {
			try {
				int quantityInCart = 0;
				if (cart.containsKey(id)) {
					quantityInCart = cart.get(id);
				}
				service.checkProductAvailable(id, quantity + quantityInCart);
				cart.computeIfPresent(id, (k, v) -> v + quantity);
				cart.putIfAbsent(id, quantity);
			} catch (RemoteException e) {
				System.out.println(splitRemoteExceptionMessage(e.getMessage()));
			}
		}
	}

	public void displayCart() {
		if (cart.isEmpty()) {
			System.out.println("Cart is empty");
		} else {
			try {
				System.out.println(service.cartDisplay(cart));
			} catch (RemoteException e) {
				System.out.println(splitRemoteExceptionMessage(e.getMessage()));
			}
		}
	}

	public void displayProduct() {
		try {
			System.out.println(service.displayProducts());
		} catch (RemoteException e) {
			System.out.println(splitRemoteExceptionMessage(e.getMessage()));
		}
	}

	public void remove(int id, int quantity) {
		if (quantity <= 0) {
			System.out.println("quantity must be > 0");
			return;
		}
		if (quantity > cart.get(id)) {
			cart.remove(id);
			System.out.println("remove product " + id +" of the shopping cart");
		} else {
			cart.compute(id, (k, v) -> v - quantity);
			System.out.println("remove " + quantity +" unit ot product " + id);
		}
	}

	public void buy(int id) {
		if (cart.isEmpty()) {
			System.out.println("Can not buy from an empty shopping cart");
		} else {
			for (Entry<Integer, Integer> items : cart.entrySet()) {
				try {
					double tmp = service.checkProductPrice(items.getKey())*items.getValue();
					double price = (double) server.convert("","EUR", bank.getCurrency(id), tmp, true, "", CurncsrvReturnRate.curncsrvReturnRateNumber, "", "");
					if(price < bank.valeurDuSolde(id)) {
						service.buyProduct(items.getKey(), items.getValue());
						bank.retraitDe(id, price);
						System.out.println("You have bought " + items.getValue() + " of the product : " + items.getKey());
					}
					else {
						System.out.println("You have not enough money to buy " + items.getValue() + " unit of product : "+items.getKey());
					}
				} catch (RemoteException e) {
					System.out.println(splitRemoteExceptionMessage(e.getMessage()));
				}
			}
			cart.clear();
		}
	}

}