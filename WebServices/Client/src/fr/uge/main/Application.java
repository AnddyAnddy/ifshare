package fr.uge.main;

import java.rmi.RemoteException;
import java.rmi.ServerException;

import javax.xml.rpc.ServiceException;

import com.currencysystem.webservices.currencyserver.CurncsrvReturnRate;
import com.currencysystem.webservices.currencyserver.CurrencyServerLocator;
import com.currencysystem.webservices.currencyserver.CurrencyServerSoap;


public class Application {
	/**
	 * @param args
	 * @throws ServerException
	 * @throws RemoteException
	 * @throws ServiceException
	 */
	public static void main(String[] args) throws ServerException, RemoteException, ServiceException {
		Console console = new Console();
		console.terminal();
	}
}
