package fr.uge.main;

import java.rmi.RemoteException;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.xml.rpc.ServiceException;

import com.currencysystem.webservices.currencyserver.CurncsrvReturnRate;
import com.currencysystem.webservices.currencyserver.CurrencyServerLocator;
import com.currencysystem.webservices.currencyserver.CurrencyServerSoap;

import fr.uge.bank.Bank;
import fr.uge.bank.BankServiceLocator;
import fr.uge.ifService.IfService;
import fr.uge.ifService.IfServiceServiceLocator;

public class Console {
	private final IfService service;
	private final ShoppingCart cart;
	private final Bank bank;
	private final CurrencyServerSoap server;
	private int id;

	public Console() throws ServiceException, RemoteException {
		this.service = new IfServiceServiceLocator().getIfService();
		this.bank = new BankServiceLocator().getBank();
		this.server = new CurrencyServerLocator().getCurrencyServerSoap();
		this.cart = new ShoppingCart(service,bank,server);
		register();
	}
	
	public void setCurrency(String currency) throws RemoteException{
		if(server.currencyExists("", currency, true)) {
			double t = (double) server.convert("", bank.getCurrency(id), currency, bank.valeurDuSolde(id), true, "", CurncsrvReturnRate.curncsrvReturnRateNumber, "", "");
			bank.setCurrency(id,currency);
			bank.setSolde(id,t);
		}
		else {
			System.out.println("Currency : " +currency +" does not exist");
		}
	}
	
	private void register() throws RemoteException {
		Scanner sc = new Scanner(System.in);
		while(true) {
			try {
				System.out.println("write your pseudo : ");
				String pseudo = sc.next("[\\w]+");
				int id = service.register(pseudo);
				bank.register(id);
				this.id = id;
				System.out.println("Welcome " + pseudo + " id : " + this.id);
				break;
			}catch (InputMismatchException e) {
				System.out.println("input mismatch" + e);
			}
		}
	}

	private void help() {
		StringBuilder sb = new StringBuilder();
		sb.append("command list : \n");
		sb.append("\tadd <ProductId(Integer)> <Quantity(Integer)>\n");
		sb.append("\tremove <ProductId(Integer)> <Quantity(Integer)>\n");
		sb.append("\tdeposit <Amount(Integer)>\n");
		sb.append("\tsetcurrency <Currency(String)>\n");
		sb.append("\tbuy\n");
		sb.append("\tsolde\n");
		sb.append("\tdisplaycart\n");
		sb.append("\tdisplayproducts\n");
		sb.append("\tstop");
		System.out.println(sb.toString());
	}

	public boolean parse() throws RemoteException {
		try {
			Scanner sc = new Scanner(System.in);
			System.out.println("enter a command : ");
			String input = sc.next("[\\w]+");
			switch (input) {
			case "add":
				cart.addProduct(sc.nextInt(), sc.nextInt());
				break;
			case "remove":
				cart.remove(sc.nextInt(), sc.nextInt());
				break;
			case "deposit":
				bank.depotDe(id, sc.nextInt());
			case "solde" :
				System.out.println("solde in account : " + bank.valeurDuSolde(id) +bank.getCurrency(id));
				break;
			case "buy":
				cart.buy(id);
				break;
			case "setcurrency":
				setCurrency(sc.next());
				break;
			case "help":
				help();
				break;
			case "displaycart":
				cart.displayCart();
				break;
			case "displayproducts":
				cart.displayProduct();
				break;
			case "stop":
				return false;
			default:
				System.out.println("invalid input : " + input);
			}
		} catch (InputMismatchException e) {
			System.out.println("input mismatch : " + e.getMessage());
		}
		return true;
	}

	public void terminal() throws RemoteException {
		System.out.println("terminal opened");
		boolean flag = true;
		while (flag) {
			flag = parse();
		}
		System.out.println("terminal closed");
	}
}
