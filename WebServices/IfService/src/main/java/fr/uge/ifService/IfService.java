package fr.uge.ifService;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import share.IClientManager;

public class IfService {
	private final IClientManager clientManager;
    private final HashMap<Integer,String> clientList = new HashMap<Integer,String>();
    private final AtomicInteger id = new AtomicInteger(0);
    
	private int nextId() {
        for (; ; ) {
            int current = id.get();
            int newValue = current + 1;
            if (id.compareAndSet(current, newValue)) {
                return current;
            }
        }
    }

    public int register(String pseudo) throws RemoteException{
        for(Map.Entry<Integer, String> elem : clientList.entrySet()){
            if(elem.getValue().equals(pseudo)){
                return elem.getKey();
            }
        }
        int tmp = nextId();
        clientList.put(tmp, pseudo);
        return tmp;
    }

	public IfService() throws MalformedURLException, RemoteException, NotBoundException {
		clientManager = (IClientManager) Naming.lookup("ClientManager");
	}

	public void buyProduct(int productId, int quantity) throws RemoteException {
		clientManager.buyProduct(productId, quantity);
	}

	public String displayProducts() throws RemoteException {
		StringBuilder sb = new StringBuilder();
		for (String product : clientManager.display()) {
			sb.append(product).append("\n");
		}
		return sb.toString();
	}
	
	public float checkProductPrice(int productId) throws RemoteException{
		return clientManager.checkProductPrice(productId);
	}

	public void checkProductAvailable(int productID, int quantity) throws RemoteException {
		clientManager.checkProductAvailable(productID, quantity);
	}

	public String cartDisplay(HashMap<Integer, Integer> idList) throws RemoteException {
		StringBuilder sb = new StringBuilder();
		for (String elem : clientManager.cartDisplay(idList)) {
			sb.append(elem).append("\n");
		}
		return sb.toString();
	}
}
