package fr.uge.bank;

public class ClientData {
	private String currency;
	private double solde;

	public ClientData(double solde, String currency) {
		this.currency = currency;
		this.solde = solde;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
}
