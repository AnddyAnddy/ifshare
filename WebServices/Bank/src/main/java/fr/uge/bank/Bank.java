package fr.uge.bank;

import java.util.HashMap;

public class Bank {
	private final HashMap<Integer, ClientData> clientList = new HashMap<>();
	


	public void register(int clientId) {
		if(clientList.containsKey(clientId)) {
			return;
		}
		clientList.put(clientId, new ClientData(0, "EUR"));
	}
	
	public void setCurrency(int clientId, String currency) {
		ClientData tmp = clientList.get(clientId);
		tmp.setCurrency(currency);
		clientList.put(clientId, tmp);
	}
	
	public String getCurrency(int clientId) {
		return clientList.get(clientId).getCurrency();
	}
	
	public void setSolde(int clientId, double amount) {
		ClientData tmp = clientList.get(clientId);
		tmp.setSolde(amount);
		clientList.put(clientId, tmp);
	}

	private void compute(int clientId, double value) {
		ClientData tmp = clientList.get(clientId);
		tmp.setSolde(tmp.getSolde()+value);
		clientList.put(clientId, tmp);
	}

	public void depotDe(int clientId, double value) {
		if (value <= 0) {
			System.out.println("value must be positive");
			return;
		}
		compute(clientId, value);
	}

	public boolean retraitDe(int clientId, double value) {
		if (value <= 0) {
			System.out.println("value must be positive");
			return false;
		}
		if (clientList.get(clientId).getSolde() < value) {
			return false;
		}
		compute(clientId, -value);
		return true;
	}

	public double valeurDuSolde(int clientId) {
		return clientList.get(clientId).getSolde();
	}

}
