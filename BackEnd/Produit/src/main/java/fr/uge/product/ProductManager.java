package fr.uge.product;

import share.Category;
import share.IProductManager;
import share.Product;
import share.State;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ProductManager extends UnicastRemoteObject implements IProductManager {

    // map des produits
    private final Map<Integer, Product> productMap = new ConcurrentHashMap<>();
    // map des quantités des produits
    private final Map<Integer, Integer> productQuantities = new ConcurrentHashMap<>();
    // liste des observers
    private final Set<Integer> productAlreadySoldOnce = new HashSet<>();
    private final Set<IProductManagerObserver> observers = new HashSet<>();
    private final AtomicInteger id = new AtomicInteger(0);

    public ProductManager() throws RemoteException {
        super();
        observers.add(new QueueProductObserver());
    }

    Set<Integer> getProductAlreadySoldOnce() {
        return productAlreadySoldOnce;
    }

    /**
     * Get the product with the same id as productID
     *
     * @param productID id of a product
     * @return Product with productID as an ID
     * @throws RemoteException rmi
     */
    public Product getProduct(int productID) throws RemoteException {
        if (!productMap.containsKey(productID)) {
            throw new IllegalArgumentException("No item with id " + productID + " in the shop");
        }
        return productMap.get(productID);
    }

    /**
     * Get the quantity for a product found with productID if present
     *
     * @param productID id of a product
     * @return the quantity of a product
     */
    public int getQuantity(int productID) {
        if (!productMap.containsKey(productID)) {
            throw new IllegalArgumentException("No item with id " + productID + " in the shop");
        }

        return productQuantities.get(productID);
    }

    void subQuantities(int productId, int quantityToSub) {
        checkProductAvailable(productId, quantityToSub);
        productQuantities.computeIfPresent(productId, (k, v) -> v - quantityToSub);
    }

    void checkProductAvailable(int productID, int quantity) {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Quantity must be positive : " + quantity);
        }
        if (!productMap.containsKey(productID)) {
            throw new IllegalArgumentException("No item with id " + productID + " in the shop");
        }
    }

    private int nextId() {
        for (; ; ) {
            int current = id.get();
            int newValue = current + 1;
            if (id.compareAndSet(current, newValue)) {
                return current;
            }
        }
    }

    /**
     * @param productID  id of a product
     * @param score      score of a product
     * @param comment    comment of a product
     * @param employeeID id of an employee
     * @throws RemoteException rmi
     */
    public void computeProduct(int productID, float score, String comment, int employeeID) throws RemoteException {
        if (!productMap.containsKey(productID)) {
            throw new IllegalArgumentException("No item with id " + productID + " in the shop");
        }

        var prod = getProduct(productID);
        prod.setScore(employeeID, score);
        prod.setComment(employeeID, comment);
        productMap.put(productID, prod);
    }

    /**
     * Create a product from name, category, state and price then add it to the manager then returns it
     *
     * @param name     name of a product
     * @param category category of a product
     * @param state    state of a product
     * @param price    price of a product
     * @return the product created with name, category, state and price
     * @throws RemoteException rmi
     */
    @Override
    public Product add(String name, Category category, State state, float price) throws RemoteException {
        Objects.requireNonNull(name);
        Objects.requireNonNull(category);

        if (price < 0) {
            throw new IllegalArgumentException("Price must be positive : " + price);
        }
        var tmp = nextId();
        var product = new Product(name, tmp, category, state, price);
        productMap.put(tmp, product);
        productQuantities.put(tmp, 0);
        observers.forEach(o -> o.onAdd(this, productMap.get(tmp)));
        return product;
    }

    /**
     * Add quantity to the product found with productID if present
     *
     * @param productID id of a product
     * @param quantity  quantity to add
     * @throws RemoteException rmi
     */
    @Override
    public void restock(int productID, int quantity) throws RemoteException {
        checkProductAvailable(productID, quantity);
        productQuantities.computeIfPresent(productID, (k, v) -> v + quantity);
        for (var o : observers) {
            o.onRestock(this, productMap.get(productID));
        }
    }

    /**
     * Sub quantity to the product found with productID if present
     *
     * @param productID id of a product
     * @param quantity  quantity to sub
     * @throws RemoteException rmi
     */
    @Override
    public void remove(int productID, int quantity) throws RemoteException {
        checkProductAvailable(productID, quantity);
        var currentQuantity = productQuantities.get(productID);
        if (currentQuantity - quantity < 0) {
            throw new IllegalArgumentException("Attempt to remove " + quantity + " of " + currentQuantity +
                    " from the product " + productID);
        }
        productQuantities.computeIfPresent(productID, (k, v) -> v - quantity);
    }


    /**
     * Put quantity at 0 to the product found with productID if present
     *
     * @param productID id of a product
     * @throws RemoteException rmi
     */
    @Override
    public void removeAll(int productID) throws RemoteException {
        checkProductAvailable(productID, 1);
        productQuantities.computeIfPresent(productID, (k, v) -> 0);
    }

    /**
     * Sub quantity of the product found with productID if present and quantity is enough.
     * Otherwise the productID and quantity is put in a FIFO queue.
     * Then put the product in the list of product already sold once for client (not for employee)
     *
     * @param productID id of a product
     * @param quantity  quantity to sub
     * @throws RemoteException rmi
     */
//     * si queue vide :
//     * check si stock suffisant : enleve la quantité voulue au stock
//     * pas suffisant : stock = 0 et put employe dans queue + quantité voulue restante (fr.uge.product.WaitingBuyer)
//     * pas vide :
//     * put employe dans queue + quantité voulue restante (fr.uge.product.WaitingBuyer)*
    @Override
    public void buy(int productID, int quantity) throws RemoteException {

        checkProductAvailable(productID, quantity);
        var quantityLeftToBuy = quantity;
        var quantityProduct = productQuantities.get(productID);
        if (quantityProduct - quantityLeftToBuy >= 0) {
            productQuantities.computeIfPresent(productID, (k, v) -> v - quantity);
            quantityLeftToBuy = 0;
        } else {
            productQuantities.compute(productID, (k, v) -> 0);
            quantityLeftToBuy -= quantityProduct;
        }
        var finalQuantityLeftToBuy = quantityLeftToBuy;
        productAlreadySoldOnce.add(productID);
        observers.forEach(o -> o.onBuy(this, productMap.get(productID), finalQuantityLeftToBuy));
    }

    /**
     * Display every product and their quantities
     *
     * @return String of products and their quantities
     */
    @Override
    public String toString() {
        System.out.println("ProduitManager{" +
                "productMap=" + productMap +
                ", productQuantities=" + productQuantities +
                '}');
        return "ProduitManager{" +
                "productMap=" + productMap +
                ", productQuantities=" + productQuantities +
                '}';
    }

    /**
     * Display every product and their quantities
     *
     * @return String of products and their quantities
     */
    public String display() throws RemoteException {
        StringBuilder sb = new StringBuilder();
        sb.append("Product list : {\n");
        for (var value : productQuantities.entrySet()) {
            sb.append("\t" + getProduct(value.getKey()) + ", quantity : " + value.getValue() + "\n");
        }
        sb.append("}\n");
        return sb.toString();
    }
}
