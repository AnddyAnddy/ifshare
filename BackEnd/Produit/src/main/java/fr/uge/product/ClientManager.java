package fr.uge.product;

import share.IClientManager;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ClientManager extends UnicastRemoteObject implements IClientManager {
    private final ProductManager productManager;

    public ClientManager(ProductManager productManager) throws RemoteException {
        this.productManager = productManager;
    }

    /**
     * Buy a product from the manager of product if the product is already sold once between employees
     *
     * @param productId id of the product
     * @param quantity  quantity wanted
     * @throws RemoteException rmi
     */
    public void buyProduct(int productId, int quantity) throws RemoteException {
        if (productManager.getProductAlreadySoldOnce().contains(productId)) {
            if (quantity > productManager.getQuantity(productId)) {
                throw new IllegalArgumentException("The product with " + productId + " as ID isn't available for the quantity given : " + quantity);
            }
            productManager.buy(productId, quantity);
        } else {
            throw new IllegalArgumentException("This product isn't available : " + productId);
        }
    }

    /**
     * Returns the description of every product already sold once between employees and their quantities
     *
     * @return description of products
     * @throws RemoteException rmi
     */
    public Set<String> display() throws RemoteException {
        var displaySet = new HashSet<String>();
        for (var idProduct : productManager.getProductAlreadySoldOnce()) {
            displaySet.add(productManager.getProduct(idProduct).toString()
                    + ", quantity : " + productManager.getQuantity(idProduct));
        }
        return displaySet;
    }

    public float checkProductPrice(int productId) throws RemoteException {
        if (!productManager.getProductAlreadySoldOnce().contains(productId)) {
            throw new IllegalArgumentException("This product isn't available : " + productId);
        }
        return productManager.getProduct(productId).getPrice();
    }

    /**
     * Check if a product is already sold once between employees
     *
     * @param productId id of a product
     * @param quantity  quantity wanted
     */
    public void checkProductAvailable(int productId, int quantity) {
        if (!productManager.getProductAlreadySoldOnce().contains(productId)) {
            throw new IllegalArgumentException("This product isn't available : " + productId);
        }
        if (productManager.getQuantity(productId) < quantity) {
            throw new IllegalArgumentException("Not enough of this product : " + productId);
        }
    }

    /**
     * Returns the description of every product from idList between employees and their quantities
     *
     * @param idList map of a productID associated to his quantity
     * @return every description of every product of idList
     * @throws RemoteException rmi
     */
    public Set<String> cartDisplay(HashMap<Integer, Integer> idList) throws RemoteException {
        var displaySet = new HashSet<String>();
        for (var elem : idList.keySet()) {
            displaySet.add(productManager.getProduct(elem).toString() + ", quantity : " + idList.get(elem));
        }
        return displaySet;
    }
}
