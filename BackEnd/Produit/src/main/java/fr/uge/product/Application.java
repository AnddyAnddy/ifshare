package fr.uge.product;

import share.IClientManager;
import share.IProductManager;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class Application {
    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(1099);
            var productManager = new ProductManager();
            IProductManager c = productManager;
            IClientManager clientManager = new ClientManager(productManager);
            Naming.rebind("ProductManager", c);
            Naming.rebind("ClientManager", clientManager);
            System.out.println("Product server open");
        } catch (Exception e) {
            System.out.println("trouble " + e);
        }
    }
}
