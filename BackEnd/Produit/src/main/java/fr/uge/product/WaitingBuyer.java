package fr.uge.product;

public class WaitingBuyer {

    private int quantity;

    public WaitingBuyer(int quantity) {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Quantity must be positive : " + quantity);
        }
        this.quantity = quantity;
    }

    /**
     * Returns the quantity that the buyer want
     *
     * @return the quantity that the buyer want
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Substracts the quantity by the quantity bought by the buyer.
     * Returns true if the buyer doesn't have anything to buy anymore.
     *
     * @param quantityBought - quantity to substract from the buyer
     * @return true if the buyer doesn't have anything to buy anymore
     */
    boolean subQuantity(int quantityBought) {
        if (quantityBought <= 0) {
            throw new IllegalArgumentException("quantityBought must be positive : " + quantityBought);
        }

        if (quantity - quantityBought < 0) {
            throw new IllegalArgumentException("Not enough quantity : " + quantity + " to substract from : " + quantityBought);
        }

        quantity -= quantityBought;
        return quantity == 0;
    }
}
