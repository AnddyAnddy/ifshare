package fr.uge.product;

import share.Product;

import java.rmi.RemoteException;
import java.util.*;

class QueueProductObserver implements IProductManagerObserver {

    private final Object lock = new Object();
    private final Map<Integer, ArrayDeque<WaitingBuyer>> productWaitingQueue = new HashMap<>();

    /**
     * When a product is added, a new queue is created for this product
     * @param productManager manager of products
     * @param product a product
     */
    @Override
    public void onAdd(ProductManager productManager, Product product) {
        synchronized (lock) {
            productWaitingQueue.put(product.getId(), new ArrayDeque<>());
        }
    }

    /**
     * When a product is bought and there is not enough stock for the product,
     * put a person and his quantity wanted(WaitingBuyer) to the queue associated
     * to the product if only quantity is more than 0
     * @param productManager the manager of product
     * @param product a product
     * @param quantity quantity left wanted
     */
    @Override
    public void onBuy(ProductManager productManager, Product product, int quantity) {
        if (quantity > 0) {
            synchronized (lock) {
                var dequeue = productWaitingQueue.get(product.getId());
                dequeue.offerLast(new WaitingBuyer(quantity));
            }
        }
    }

    /**
     * When a product is restocked, if the queue associated is empty it does nothing.
     * Otherwise it subs the quantity of a product from every person and their quantity wanted
     * (WaitingBuyer) in the queue to the point where there is no more quantity left for the product
     * @param productManager manager of products
     * @param product a product
     * @throws RemoteException rmi
     */
    @Override
    public void onRestock(ProductManager productManager, Product product) throws RemoteException {
        var productID = product.getId();
        var productQuantityLeft = productManager.getQuantity(productID);
        while (!productWaitingQueue.get(productID).isEmpty() && productQuantityLeft > 0) {
            synchronized (lock) {
                var buyer = productWaitingQueue.get(productID).peekFirst();
                if (buyer == null) {
                    throw new IllegalStateException("No buyer in dequeue");
                }
                // quantityBought : qu'est qu'on doit retirer à la personne en attente en terme de quantité après restock
                // si la quantité achetée est inférieure ou égale du stock, on prend la quantité du buyer
                // sinon ça veut dire qu'on prend le reste du stock
                var quantityBought = buyer.getQuantity() - productQuantityLeft <= 0 ? buyer.getQuantity() : productQuantityLeft;
                if (buyer.subQuantity(quantityBought)) {
                    productWaitingQueue.get(productID).pollFirst();
                }
                productManager.subQuantities(productID, quantityBought);
            }
        }
    }
}
