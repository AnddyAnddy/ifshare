package fr.uge.product;

import share.Product;

import java.rmi.RemoteException;

interface IProductManagerObserver {

    default void onAdd(ProductManager productManager, Product product) {
    }

    default void onBuy(ProductManager productManager, Product product, int quantity) {
    }

    default void onRestock(ProductManager productManager, Product product) throws RemoteException {
    }

}
