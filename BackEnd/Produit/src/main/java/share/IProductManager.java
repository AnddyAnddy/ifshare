package share;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IProductManager extends Remote {

    Product add(String name, Category category, State state, float price) throws RemoteException;

    void restock(int productID, int quantity) throws RemoteException;

    void remove(int productID, int quantity) throws RemoteException;

    void removeAll(int productID) throws RemoteException;

    void buy(int productID, int quantity) throws RemoteException;

    String display() throws RemoteException;

    Product getProduct(int productId) throws RemoteException;

    void computeProduct(int productID, float score, String comment, int employeeID) throws RemoteException;
}
