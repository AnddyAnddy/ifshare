package share;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Objects;

public class Product implements Serializable {
    private final String name;
    private final int id;
    private float price;
    private final HashMap<Integer, String> comment;
    private final HashMap<Integer, Float> score;
    private final Category category;
    private final State state;

    public Product(String name, int id, Category category, State state, float price) {
        if (price < 0) {
            throw new IllegalArgumentException("price cannot be negative");
        }
        this.name = Objects.requireNonNull(name);
        this.id = id;
        this.comment = new HashMap<>();
        this.score = new HashMap<>();
        this.category = Objects.requireNonNull(category);
        this.state = Objects.requireNonNull(state);
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setPrice(float price) throws IllegalArgumentException {
        if (price < 0) {
            throw new IllegalArgumentException("price cannot be negative");
        }
        this.price = price;
    }

    public void setComment(int idPers, String comment) {
        Objects.requireNonNull(comment);
        this.comment.put(idPers, comment);
    }

    public void setScore(int idPers, float score) throws IllegalArgumentException {
        if (score < 0 || score > 5) {
            throw new IllegalArgumentException("score cannot be negative");
        }
        this.score.put(idPers, score);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Id : ").append(id)
                .append(", Name : ").append(name)
                .append(", Price : ").append(price)
                .append(", State : ").append(state)
                .append(", Category : ").append(category);
        if (score.isEmpty()) {
            sb.append(", No grade");
        } else {
            sb.append(", Grade : ");
            sb.append(score.values().stream().reduce(Float::sum).get() / (float) score.size());
        }
        sb.append(", Comments : ");
        for (var c : comment.values()) {
            sb.append(", \"").append(c).append("\"");
        }
        return String.join(" ", sb);
    }

    public float getPrice() {
        return price;
    }
}
