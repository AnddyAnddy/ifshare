package share;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Set;

public interface IClientManager extends Remote {

    void buyProduct(int productId, int quantity) throws RemoteException;

    Set<String> display() throws RemoteException;

    void checkProductAvailable(int productID, int quantity)throws RemoteException;

    Set<String> cartDisplay(HashMap<Integer,Integer> idList) throws RemoteException;

    float checkProductPrice(int productId) throws RemoteException;
}
