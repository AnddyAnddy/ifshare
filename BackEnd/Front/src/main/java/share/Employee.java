package share;

import java.io.Serializable;

public class Employee implements Serializable {
    private final String firstName;
    private final String lastName;
    private final int id;

    public Employee(String firstName, String lastName, int id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
