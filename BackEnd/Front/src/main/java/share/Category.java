package share;

public enum Category {
    LITERATURE, BEAUTY, FOOD, LUXURY, ACCESSORY
}
