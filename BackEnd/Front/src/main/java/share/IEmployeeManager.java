package share;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IEmployeeManager extends Remote {

    int register(String firstName, String lastName) throws RemoteException;

    Employee getEmployee(int id) throws RemoteException;
}
