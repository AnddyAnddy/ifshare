package fr.uge.front;

import share.IEmployeeManager;
import share.IProductManager;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Application {

    private static int register(IEmployeeManager employeeManager) throws RemoteException {
        System.out.println(" firstname lastname : ");
        while (true) {
            try {
                var sc = new Scanner(System.in);
                var firstName = sc.next("[\\w]+");
                var lastName = sc.next("[\\w]+");
                var id = employeeManager.register(firstName, lastName);
                System.out.println("Welcome " + firstName + " " + lastName + " " + id);
                return id;
            } catch (InputMismatchException e) {
                System.out.println("try again");
            }
        }
    }

    public static void main(String[] args) {
        try {
            IProductManager c = (IProductManager) Naming.lookup("ProductManager");
            IEmployeeManager e = (IEmployeeManager) Naming.lookup("rmi://localhost:2000/EmployeeManager");
            CommandSystem sys = new CommandSystem(c, register(e));
            sys.terminal();
        } catch (Exception e) {
            System.out.println("Exception" + e);
        }
    }
}
