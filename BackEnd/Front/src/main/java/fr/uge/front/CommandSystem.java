package fr.uge.front;

import share.*;

import java.rmi.RemoteException;
import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

public class CommandSystem {
    private final IProductManager productManager;
    private final int employeeId;

    public CommandSystem(IProductManager productManager, int employeeId) {
        this.productManager = Objects.requireNonNull(productManager);
        this.employeeId = employeeId;
    }


    private void addProduct(String name, String category, String state, Float price, int quantity) throws RemoteException {
        try {
            var product = productManager.add(name, Category.valueOf(category), State.valueOf(state), price);
            productManager.restock(product.getId(), quantity);
            System.out.println("add " + product);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    private void restock(int idProd, int quantity) throws RemoteException {
        try {
            productManager.restock(idProd, quantity);
            System.out.println("restock " +quantity + " unit of product : " +idProd );
        } catch (IllegalArgumentException t) {
            System.out.println(t.getMessage());
        }
    }

    private void remove(int idProd, int quantity) throws RemoteException {
        try {
            productManager.remove(idProd, quantity);
            System.out.println("remove " + quantity + " unit of product : " + idProd);
        } catch (IllegalArgumentException t) {
            System.out.println(t.getMessage());
        }
    }

    private void removeAll(int idProd) throws RemoteException {
        try {
            productManager.removeAll(idProd);
            System.out.println("remove product " + idProd + " stock");
        } catch (IllegalArgumentException t) {
            System.out.println(t.getMessage());
        }
    }

    private void buy(int idProd, int quantity) throws RemoteException {
        try {
            productManager.buy(idProd, quantity);
            System.out.println("bought " + quantity + " of the product " + idProd);
            addCommentAndScore(idProd);
        } catch (IllegalArgumentException t){
            System.out.println(t.getMessage());
        }
    }

    private void addCommentAndScore(int productId) throws RemoteException {
        var sc = new Scanner(System.in);
        while (true) {
            System.out.println("would you like to comment and grade the product : (Y or N)");
            var tmp = sc.next();
            if (tmp.equals("Y")) {
                System.out.println("write your comment : ");
                break;
            }
            if (tmp.equals("N")) {
                return;
            }
        }
        sc = new Scanner(System.in);
        var comment = sc.next();
        System.out.println("write the grade (0-5) :");
        while (true) {
            try {
                sc = new Scanner(System.in);
                var rate = sc.next("[0-4](\\.[0-9])?|5");
                productManager.computeProduct(productId, Float.parseFloat(rate), comment, employeeId);
                break;
            } catch (InputMismatchException e) {
                System.out.println("invalid input : " + e.getMessage());
            }
        }

    }

    private void help() {
        StringBuilder sb = new StringBuilder();
        sb.append("command list : \n");
        sb.append("\tadd <ProductName(String)> <Category(String)> <State(String)> <Price(Float)> <Quantity(Integer)>\n");
        sb.append("\trestock <ProductId(Integer)> <Quantity(Integer)>\n");
        sb.append("\tremove <ProductId(Integer)> <Quantity(Integer)>\n");
        sb.append("\tremoveall <ProductId(Integer)>\n");
        sb.append("\tbuy <ProductId(Integer)> <Quantity(Integer)>\n");
        sb.append("\tdisplay\n");
        sb.append("\tstop");
        System.out.println(sb.toString());
    }

    public boolean parse() {

        try {
            var sc = new Scanner(System.in);
            System.out.println("enter a command : ");
            var input = sc.next("[\\w]+");
            switch (input) {
                case "add":
                    addProduct(sc.next("[\\w.-]+"), sc.next(), sc.next(), sc.nextFloat(), sc.nextInt());
                    break;
                case "restock":
                    restock(sc.nextInt(), sc.nextInt());
                    break;
                case "remove":
                    remove(sc.nextInt(), sc.nextInt());
                    break;
                case "removeall":
                    removeAll(sc.nextInt());
                    break;
                case "buy":
                    buy(sc.nextInt(), sc.nextInt());
                    break;
                case "help":
                    help();
                    break;
                case "display":
                    System.out.println(productManager.display());
                    break;
                case "stop":
                    return false;
                default:
                    System.out.println("invalid input : " + input);
            }
        } catch (InputMismatchException | RemoteException e) {
            System.out.println("input mismatch : " + e.getMessage());
        }
        return true;
    }

    public void terminal() throws RemoteException {
        System.out.println("terminal opened\n");
        boolean flag = true;
        while (flag) {
            flag = parse();
        }
        System.out.println("terminal closed\n");
    }


}
