package fr.uge.employee;

import share.IEmployeeManager;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class Application {
    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(2000);
            IEmployeeManager c = new EmployeeManager();
            Naming.rebind("rmi://localhost:2000/EmployeeManager", c);
            System.out.println("Employee server open");
        } catch (Exception e) {
            System.out.println("trouble " + e);
        }


    }
}
