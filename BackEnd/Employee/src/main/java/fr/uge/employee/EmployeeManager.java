package fr.uge.employee;

import share.Employee;
import share.IEmployeeManager;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class EmployeeManager extends UnicastRemoteObject implements IEmployeeManager {

    private final Map<Integer, Employee> employeeMap = new HashMap<>();
    private final AtomicInteger id = new AtomicInteger(0);

    public EmployeeManager() throws RemoteException {
        super();
    }

    private int nextId() {
        for (; ; ) {
            int current = id.get();
            int newValue = current + 1;
            if (id.compareAndSet(current, newValue)) {
                return current;
            }
        }
    }

    @Override
    public int register(String firstName, String lastName) throws RemoteException {
        Objects.requireNonNull(lastName);
        Objects.requireNonNull(firstName);

        for (var elem : employeeMap.entrySet()) {
            var tmp = elem.getValue();
            if (tmp.getFirstName().equals(firstName) && tmp.getLastName().equals(lastName)) {
                return elem.getKey();
            }
        }
        var tmp = nextId();
        employeeMap.put(tmp, new Employee(firstName, lastName, tmp));
        return tmp;
    }

    @Override
    public Employee getEmployee(int id) throws RemoteException {
        if (!employeeMap.containsKey(id)) {
            throw new IllegalArgumentException("This is is not registered : " + id);
        }

        return employeeMap.get(id);
    }
}
